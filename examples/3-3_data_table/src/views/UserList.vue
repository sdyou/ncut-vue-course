<template>
  <div>
    <el-row>
      <el-form
        ref="form"
        :model="form"
        label-position="top">
        <el-row :gutter="10">
          <!--學校-->
          <el-col :span="6">
            <el-form-item label="學校" prop="school_snoo">
              <el-select
                v-model="form.school_sno"
                placeholder="選擇學校"
                filterable>
                <el-option
                  v-for="(item, index) in schoolOptions"
                  :key="index"
                  :label="item.label"
                  :value="item.value">
                </el-option>
              </el-select>
            </el-form-item>
          </el-col>
          <!--帳號-->
          <el-col :span="6">
            <el-form-item label="帳號" prop="user_id">
              <el-input
                v-model="form.user_id"
                placeholder="輸入帳號">
              </el-input>
            </el-form-item>
          </el-col>
          <!--姓名-->
          <el-col :span="12">
            <el-form-item label="姓名" prop="user_name">
              <el-input
                v-model="form.user_name"
                placeholder="輸入帳號">
              </el-input>
            </el-form-item>
          </el-col>
        </el-row>
        <el-row :gutter="10">
          <!--日期起迄-->
          <el-col :span="6">
            <el-form-item label="開始日期" prop="start_date">
              <el-date-picker
                v-model="form.start_date"
                value-format="yyyy-MM-dd"
                type="date"
                :clearable="false"
                label="開始日期"
                splaceholder="開始日期">
              </el-date-picker>
            </el-form-item>
          </el-col>
          <el-col :span="6">
            <el-form-item label="結束日期" prop="end_date">
              <el-date-picker
                v-model="form.end_date"
                value-format="yyyy-MM-dd"
                type="date"
                :clearable="false"
                label="結束日期"
                splaceholder="結束日期">
              </el-date-picker>
            </el-form-item>
          </el-col>
        </el-row>
        <el-row>
          <!--按鈕-->
          <el-button
            type="primary"
            @click="doSearch">
            查詢
          </el-button>
          <el-button
            :disabled="!tableData.length"
            @click="exportExcel">
            匯出
          </el-button>
        </el-row>
      </el-form>
    </el-row>
    <el-row>
      <el-table
        v-loading="isTableDataLoading"
        :data="currentTableData"
        class="dataTable">
        <el-table-column
          v-for="(col,index) in searchRecordFormat"
          :key="index"
          :fixed="col.fixed==true?'fixed':null"
          :prop="col.index"
          :label="col.title"
          :width="col.width">
        </el-table-column>
      </el-table>
    </el-row>
    <el-row class="block pagger">
      <el-pagination
        background
        layout="prev, pager, next"
        :current-page="pagger.current"
        :total="tableDataCount"
        :page-size="pagger.per"
        @current-change="handlePageSelect">
      </el-pagination>
    </el-row>
  </div>
</template>

<script>
import moment from 'moment'
import api from '@/api/index.js'
import searchRecordFormat from '@/data/searchRecordFormat.js'
import roleOptions from '@/data/roleOptions.js'
/*
  今天往前一個月
const defaultStartDate = moment().subtract(1, 'months').format('YYYY-MM-DD')
*/
// 獲取當前月第一天 modified by Iris on 2019/10/24
const defaultStartDate = moment().startOf('month').format('YYYY-MM-DD')
const defaultEndDate = moment().format('YYYY-MM-DD')

export default {
  data () {
    return {
      roleOptions,
      schoolOptions: [],
      form: {
        school_sno: 0,
        user_id: '',
        start_date: defaultStartDate,
        end_date: defaultEndDate
      },
      searchResultList: [],
      pagger: {
        current: 1,
        per: 10
      },
      tableData: [],
      searchRecordFormat: searchRecordFormat,
      isTableDataLoading: false
    }
  },
  computed: {
    currentTableData () {
      const startNo = ((this.pagger.current - 1) * this.pagger.per)
      const endNo = startNo + this.pagger.per - 1
      // 篩選
      let newTableData = this.tableData.filter((d, i) => {
        return i >= startNo && i <= endNo
      })
      return newTableData
    },
    tableDataCount () {
      return this.tableData.length
    },
  },
  methods: {
    async doSearch () {
      this.handlePageSelect(1)

      let query = {
        start_date: this.form.start_date,
        end_date: this.form.end_date,
        school_sno: this.form.school_sno,
        user_id: this.form.user_id,
        user_name: this.form.user_name
      }
      
      this.$router.push({
        name: 'userList',
        query
      })
    },
    handlePageSelect (value) {
      this.$set(this.pagger, 'current', value)
    },
    exportExcel () {
      import('@/vendor/Export2Excel.js').then(excel => {
        if (!this.tableData.length) {
          return
        }
        const data = this.tableData.map(d => {
          return this.searchRecordFormat.map(f => d[f.index] || '')
        })
        const header = this.searchRecordFormat.map(f => f.title)
        const dateString = moment().format('YYYY-MM-DD')

        excel.export_json_to_excel({
          header, // 表頭
          data, // 資料
          filename: `document_${dateString}`, // 非必填
          autoWidth: true, // 非必填
          bookType: 'xlsx' // 非必填
        })
      })
    },
  },
  created () {
    api.schoolsGet()
      .then(d => {
        this.schoolOptions = d.payload
      })
  },
  //Watch 監視某個值，當這個值變動的時候，就去做某些事情
  watch: {
    '$route': {
      handler (d) {
        let params = this.form
        if (d.params) {
          params = d.params
        }
        this.isTableDataLoading = true
        api.usersGet(params)
          .then(result => {
            this.isTableDataLoading = false
            if (result.payload && result.payload.length) {
              // 修正格式
              let newTableData = result.payload.map(d => {
                if (d.account_active != null) {
                  d.account_active = d.account_active ? '是' : '否'
                }
                return d
              })
              // 寫入
              this.tableData = result.payload
            } else {
              this.tableData = []
            }
          })
      },
      immediate: true
    },
    
  }
}
</script>

<style scoped lang="scss">

.el-form-item {
  text-align: left;
}

.el-select {
  width: 100%;
}

.el-input {
  width: 100% !important;
}

.pagger {
  text-align:center;
  margin-top: 30px;
  margin-bottom: 80px;
}
</style>
