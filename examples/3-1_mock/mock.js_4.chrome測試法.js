
//step1 在chrome console貼上以下：

var script = document.createElement("script");
script.setAttribute("src", "https://cdn.bootcss.com/Mock.js/1.0.1-beta3/mock-min.js");
script.addEventListener('load', function() {
var script = document.createElement("script");
document.body.appendChild(script);
}, false);
document.body.appendChild(script);
    
//step2

Mock.mock("https://choose.blueplanet.com.tw/api/v1/search/1/20", {
    "code":200,
    "payload":{
        "articles|20":[{
            "author":"@cname",
            "commentLevel1Count":0,
            "commentLevel2Count":0,
            "commentMention|1":false,
            "commentTotalCount":0,
            "content":"@cparagraph",
            "contentMention|1":false,
            "datetime_pub":"@datetime('yyyy-MM-dd A HH:mm:ss')",
            "dislike":0,
            "interaction":{"like|0-10": 0, "love|0-10": 0, "haha|0-10": 0, "wow|0-10": 0, "sad|0-10": 0, "angry|0-10": 0},
            "length":232,
            "like":0,
            "match":[],
            "media":"中廣中國廣播公司",
            "origin|1":["news","facebook","ptt"],
            "popularity":0,
            "score|1":[0,15,-10,3],
            "title":"@ctitle(5,20)",
            "titleMention":false,
            "url":"@url",
            "_id":"@guid"
        }],
        "pages":"1",
        "perPage":"20",
        "query":"\"__ALL__\""
    }
});

