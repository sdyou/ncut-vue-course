export default [
  {
    "label": "基隆市",
    "value": "01"
  },
  {
    "label": "台北市",
    "value": "02"
  },
  {
    "label": "新北市",
    "value": "03"
  },
  {
    "label": "桃園市",
    "value": "04"
  },
  {
    "label": "新竹市",
    "value": "05"
  },
  {
    "label": "新竹縣",
    "value": "06"
  },
  {
    "label": "苗栗縣",
    "value": "07"
  },
  {
    "label": "台中市",
    "value": "08"
  },
  {
    "label": "彰化縣",
    "value": "09"
  },
  {
    "label": "南投縣",
    "value": "10"
  },
  {
    "label": "雲林縣",
    "value": "11"
  },
  {
    "label": "嘉義市",
    "value": "12"
  },
  {
    "label": "嘉義縣",
    "value": "13"
  },
  {
    "label": "台南市",
    "value": "14"
  },
  {
    "label": "高雄市",
    "value": "15"
  },
  {
    "label": "屏東縣",
    "value": "16"
  },
  {
    "label": "台東縣",
    "value": "17"
  },
  {
    "label": "花蓮縣",
    "value": "18"
  },
  {
    "label": "宜蘭縣",
    "value": "19"
  },
  {
    "label": "澎湖縣",
    "value": "20"
  },
  {
    "label": "金門縣",
    "value": "21"
  },
  {
    "label": "連江縣",
    "value": "22"
  }
]
