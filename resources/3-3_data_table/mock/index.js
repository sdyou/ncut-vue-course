import Mock from 'mockjs'
import roleOptions from '@/data/roleOptions.js'
import schoolOptions from '@/data/schoolOptions.js'

Mock.setup({
  timeout: '200-600'
})

// /api/users.get
Mock.mock(/api\/v1\/users.get/, function (options) {
  let request = JSON.parse(options.body)
  console.log(request) // request body（參數）
  const roleNames = roleOptions.map(d => d.label)
  const schoolNames = schoolOptions.map(d => d.label)
  return Mock.mock({
    success: true,
    message: '',
    "payload|20-55": [
      {
        "user_sno|+1": 0, // 使用者sno
        "user_id": "@last",
        "user_name": "@cname", // 使用者姓名
        "role_sno|1-3": 0, // 角色sno
        "role_name|1": roleNames, // 角色名稱
        "email": "@email",
        "school_sno|1-9": 0,
        "school_name|1": schoolNames,
        "unit_sno|1-10": 0, // 單位sno
        "unit_name": "@cname" + "單位", // 單位名稱
        "apply_datetime": "@date('yyyy-MM-dd HH:mm:ss')", // 帳戶申請時間
        "account_active|1-2": true // 帳號是否啟用
      }
    ]
  })

})

// /api/schools.get
Mock.mock(/api\/v1\/schools.get/, function (options) {
  return Mock.mock({
    success: true,
    message: '',
    payload: schoolOptions
  })
})
